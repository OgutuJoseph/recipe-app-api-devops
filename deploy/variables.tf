variable "prefix" {
    default = "raad"
}

variable "project" {
    default = "recipe-app-api-devops"
}

variable "contact" {
    default = "onyango.josef@gmail.com"
}